var $ = function (id) {
    return document.getElementById(id);
 };


window.onload = function () {
 //assign button behaviour to functions
  $('create').onclick = myCalendar;
  $('reset').onclick = clearCalendar;
  };
//User input function (calendar creation based on user inputs)
function myCalendar(){

	//assign var year to the user input value 
	var year = $('year').value
	
	//assign var month to the user input value
	var month = $('month').value
	
	//invoke new calendar
	new Calendar("cal", year, month);

}
//Calendar creator function
function Calendar(id, year, month) {
 
  //assign var elem to the div in which calendar will be drawn. 
  //In this case, #cal
  var elem = $('cal')
  
  // (1) Get Date: month -1 because counter starts at 0. So if you enter 12, the date object will look for a 13th month. So minus 1 so if you enter 12, you get dec. 
  var mon = month - 1  
  
  var d = new Date(year, mon)
  
  //start table drawing
  var table = ['<table><tr>']

  // (2) get start day of month and fill first row -> lignes
  //  0  1  2  3  4  5  6
  for (var i=0; i<d.getDay(); i++) {
    table.push('<td></td>')
  }
  
  // main body (3) //fill in rest of rows based on month
  while(d.getMonth() == mon) {
    table.push('<td id =td'+d.getDate()+'>'+d.getDate()+'</td>')   //pour chd td --> ajouter click event --> ajouter un ID à chq
	
 // (4)if 7 days in the week to be filled- create rows. 
    if (d.getDay() % 7 == 6) {   
      table.push('</tr><tr>')
    }
	
 //set date: add 1 because counter starts at 0, but our days start at 1..
 //so add 1.
    d.setDate(d.getDate()+1)  
  }
  
  // (5) if less than 7 days, blank spaces
  for (var i=d.getDay(); i<7; i++) {
    table.push('<td></td>')
  }
  // table closure
  table.push('</tr></table>')
  
  //join all rows created above to create complete table and draw.
  elem.innerHTML = table.join('\n')

  var alertDelay = 100; 
  setTimeout( addEventListener, alertDelay);
}

//Reset function
function clearCalendar(){

	//reset input value for year
	$('year').value='';
	
	//reset input value for month
	$('month').value='';
	
	//clear calendar table
	$('cal').innerHTML='';
}

function addEventListener(){

  //on doit récupérer tous les élements de la table pour y ajouter un event listener
  var table = document.getElementById("cal");
 //--> on obtient toutes les cases
  for (var i = 0, row; row = table.rows[i]; i++) {
     //iterate through rows
     //rows would be accessed using the "row" variable assigned in the for loop*
     for (var j = 0, col; col = row.cells[j]; j++) {
       //iterate through columns
       //columns would be accessed using the "col" variable assigned in the for loop
       //console.log(col.innerHTML); //récupére tous les contenus
       var number = col.innerHTML;
       col.addEventListener("click",function(event){clickEvent(event)},false);
     }  
  }

}

function clickEvent(event){
  var test = document.getElementById('span'+event.target.innerText)
  if(test)
  {
    //affiche toute la ligne du calendirer --> doit sélectionner le bon numero
    var last_RDV = localStorage.getItem("RDV"+event.srcElement.innerText);
    alert(last_RDV);
  }
  else 
  {
    var new_RDV = window.prompt("Ajoutez un RDV le "+event.target.innerText,"Nom;heure");
    if(new_RDV==null || new_RDV == "")
      {
        alert("RDV non enregistré")
        return 0
      }
    //console.log(new_RDV)
    //Ajouter un ronf  de couleur pour signaler qu'on a un RDV à une certaine date
    localStorage.setItem("RDV"+event.target.innerText, new_RDV);
    document.getElementById('td'+event.target.innerText).innerHTML += "<span class='dot' id=span"+event.target.innerText+" style='height: 20px;width: 20px; background-color: white;border-radius: 50%;display: inline-block;'></span>"
  }
}

