//Initialisation
document.getElementById("mySidenav").addEventListener("click", toggleNav);

//Sidenav functions

//If open, close with menu button and vice versa
function toggleNav(){
  navSize = document.getElementById("mySidenav").style.width;
  if (navSize == "250px") {
       return closeSideNav();
  }
return openSideNav();
}
/* Set the width of the side navigation to 250px and the left margin of the page content to 250px and add a black background color to body */
function openSideNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("Donnees").style.marginLeft = "250px";
  document.getElementById("Donnees").style.opacity = 0.5;
  document.getElementsByClassName("search-box")[0].style.opacity = 0.5;
}

/* Set the width of the side navigation to 0 and the left margin of the page content to 0, and the background color of body to white */
function closeSideNav() {
  document.getElementById("mySidenav").style.width = "0px";
  document.getElementById("Donnees").style.marginLeft = "0px";
  document.getElementById("Donnees").style.opacity = 1;
  document.getElementsByClassName("search-box")[0].style.opacity = 1;
}

function searchData(){
  var input = document.getElementById("search").value;
  if (input == "Bernadette Paulint")
  {
    document.getElementById("Donnees").style.display = "block";
    document.getElementsByClassName("search-box")[0].style.top = "85%"
    document.getElementById("search").value = ""
  }
  else 
  {
    document.getElementById("Donnees").style.display = "none";
    document.getElementsByClassName("search-box")[0].style.top = "50%"
    document.getElementById("search").value = "" 
    alert('Invalid Input.');
  }
}

function closeData(){
  document.getElementById("Donnees").style.display = "none";
  document.getElementsByClassName("search-box")[0].style.top = "50%"
}

// Get the input field
var input = document.getElementById("search");

// Execute a function when the user releases a key on the keyboard
input.addEventListener("keyup", function(event) {
  // Number 13 is the "Enter" key on the keyboard
  if (event.keyCode === 13 && input.value != "") {
    // Cancel the default action, if needed
    event.preventDefault();
    // Trigger the button element with a click
    document.getElementById("btn-search").click();
  }
});

