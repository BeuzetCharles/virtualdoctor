document.getElementById("mySidenav").addEventListener("click", toggleNav);

//If open, close with menu button and vice versa
function toggleNav(){
    navSize = document.getElementById("mySidenav").style.width;
    if (navSize >= "1px") 
    {
        return closeSideNav();
    }
    else
    {
      return openSideNav();
    }
}
/* Set the width of the side navigation to 250px and the left margin of the page content to 250px and add a black background color to body */
function openSideNav() {
  if (document.getElementsByClassName("general")[0].style.width <"1600px")
  {
    document.getElementById("mySidenav").style.width = "200px";
    document.getElementsByClassName("general")[0].style.opacity = "0.3";
    document.getElementsByClassName("result")[0].style.marginLeft = "250px";
    document.getElementsByClassName("box")[0].style.marginLeft = "150px";
  }
  else
  {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementsByClassName("general")[0].style.opacity = "0.3";
    document.getElementsByClassName("result")[0].style.marginLeft = "350px";
    document.getElementsByClassName("box")[0].style.marginLeft = "350px";
  }
}

/* Set the width of the side navigation to 0 and the left margin of the page content to 0, and the background color of body to white */
function closeSideNav() {
  document.getElementById("mySidenav").style.width = "0";
  //document.getElementById("main").style.marginLeft = "0";
  document.getElementsByClassName("general")[0].style.opacity = "1";
  document.getElementsByClassName("result")[0].style.marginLeft = "10%";
  document.body.style.backgroundColor = "white";
  //document.getElementsByClassName("box")[0].style.backgroundColor="white";
}

